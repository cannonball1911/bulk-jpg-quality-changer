﻿using System;
using System.Diagnostics;

/* ETACalculator.cs, BulkJPGQualityChanger
 * Calculates the estimated time to loop through a file array
 * Kai Sackl, 04.06.2020
 */

namespace BulkJPGQualityChanger
{
    internal class ETACalculator
    {
        /// <summary>
        /// Count of all files to process
        /// </summary>
        private readonly int filesToProcess;
        /// <summary>
        /// Number of currently processed file
        /// </summary>
        private int currentlyProcessedFile;
        private double completedProcessedFilesTimeSumInSeconds;
        private readonly Stopwatch stopwatch;

        /// <summary>
        /// Gets the estimated process time in seconds
        /// (Call on end of a loop; Call only once in a loop)
        /// </summary>
        public int EstimatedProcessTimeInSeconds => CalculateEstimatedProcessTimeInSeconds();

        /// <summary>
        /// Creates an ETACalculator
        /// </summary>
        /// <param name="filesToProcess">Count of files to process</param>
        public ETACalculator(int filesToProcess)
        {
            this.filesToProcess = filesToProcess;
            currentlyProcessedFile = 0;
            completedProcessedFilesTimeSumInSeconds = 0.0;
            stopwatch = new Stopwatch();
        }

        /// <summary>
        /// Creates an ETACalculator
        /// </summary>
        /// <param name="fileArray">Array of files</param>
        public ETACalculator(string[] fileArray) : this(fileArray.Length) { }

        /// <summary>
        /// Start the calculation
        /// (Put it on beginning of a loop)
        /// </summary>
        public void Begin()
        {
            stopwatch.Reset();
            stopwatch.Start();
            currentlyProcessedFile++;
        }

        /// <summary>
        /// Calculates the estimated time in seconds
        /// </summary>
        private int CalculateEstimatedProcessTimeInSeconds()
        {
            stopwatch.Stop();
            completedProcessedFilesTimeSumInSeconds += (double)stopwatch.ElapsedMilliseconds / 1000;
            double averageProcessTimeInSeconds = completedProcessedFilesTimeSumInSeconds / currentlyProcessedFile;
            return (int)Math.Round((filesToProcess - currentlyProcessedFile) * averageProcessTimeInSeconds, 0);
        }
    }
}
