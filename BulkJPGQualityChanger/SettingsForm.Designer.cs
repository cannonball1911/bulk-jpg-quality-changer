﻿namespace BulkJPGQualityChanger
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settingsSeperator = new System.Windows.Forms.Label();
            this.LblSettings = new System.Windows.Forms.Label();
            this.BtnSettingsThumbnail = new System.Windows.Forms.Button();
            this.BtnSettingsImage = new System.Windows.Forms.Button();
            this.BtnSettingsConvert = new System.Windows.Forms.Button();
            this.CbxGenerateThumbnails = new System.Windows.Forms.CheckBox();
            this.CbxUseCustomOutputFolder = new System.Windows.Forms.CheckBox();
            this.TbxCustomFolder = new System.Windows.Forms.TextBox();
            this.BtnSelectCustomFolder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // settingsSeperator
            // 
            this.settingsSeperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.settingsSeperator.Location = new System.Drawing.Point(114, 42);
            this.settingsSeperator.Name = "settingsSeperator";
            this.settingsSeperator.Size = new System.Drawing.Size(2, 399);
            this.settingsSeperator.TabIndex = 0;
            // 
            // LblSettings
            // 
            this.LblSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSettings.Location = new System.Drawing.Point(12, 9);
            this.LblSettings.Name = "LblSettings";
            this.LblSettings.Size = new System.Drawing.Size(93, 30);
            this.LblSettings.TabIndex = 1;
            this.LblSettings.Text = "Settings";
            this.LblSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnSettingsThumbnail
            // 
            this.BtnSettingsThumbnail.FlatAppearance.BorderSize = 0;
            this.BtnSettingsThumbnail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSettingsThumbnail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSettingsThumbnail.Location = new System.Drawing.Point(1, 126);
            this.BtnSettingsThumbnail.Name = "BtnSettingsThumbnail";
            this.BtnSettingsThumbnail.Size = new System.Drawing.Size(113, 35);
            this.BtnSettingsThumbnail.TabIndex = 3;
            this.BtnSettingsThumbnail.Text = "Thumbnail";
            this.BtnSettingsThumbnail.UseVisualStyleBackColor = false;
            this.BtnSettingsThumbnail.Click += new System.EventHandler(this.BtnSettingsThumbnail_Click);
            // 
            // BtnSettingsImage
            // 
            this.BtnSettingsImage.FlatAppearance.BorderSize = 0;
            this.BtnSettingsImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSettingsImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSettingsImage.Location = new System.Drawing.Point(1, 84);
            this.BtnSettingsImage.Name = "BtnSettingsImage";
            this.BtnSettingsImage.Size = new System.Drawing.Size(113, 35);
            this.BtnSettingsImage.TabIndex = 2;
            this.BtnSettingsImage.Text = "Image";
            this.BtnSettingsImage.UseVisualStyleBackColor = false;
            this.BtnSettingsImage.Click += new System.EventHandler(this.BtnSettingsImage_Click);
            // 
            // BtnSettingsConvert
            // 
            this.BtnSettingsConvert.FlatAppearance.BorderSize = 0;
            this.BtnSettingsConvert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSettingsConvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSettingsConvert.Location = new System.Drawing.Point(1, 43);
            this.BtnSettingsConvert.Name = "BtnSettingsConvert";
            this.BtnSettingsConvert.Size = new System.Drawing.Size(113, 35);
            this.BtnSettingsConvert.TabIndex = 1;
            this.BtnSettingsConvert.Text = "Convert";
            this.BtnSettingsConvert.UseVisualStyleBackColor = false;
            this.BtnSettingsConvert.Click += new System.EventHandler(this.BtnSettingsConvert_Click);
            // 
            // CbxGenerateThumbnails
            // 
            this.CbxGenerateThumbnails.AutoSize = true;
            this.CbxGenerateThumbnails.Location = new System.Drawing.Point(145, 53);
            this.CbxGenerateThumbnails.Name = "CbxGenerateThumbnails";
            this.CbxGenerateThumbnails.Size = new System.Drawing.Size(127, 17);
            this.CbxGenerateThumbnails.TabIndex = 4;
            this.CbxGenerateThumbnails.TabStop = false;
            this.CbxGenerateThumbnails.Text = "Generate Thumbnails";
            this.CbxGenerateThumbnails.UseVisualStyleBackColor = true;
            this.CbxGenerateThumbnails.CheckedChanged += new System.EventHandler(this.CbxGenerateThumbnails_CheckedChanged);
            // 
            // CbxUseCustomOutputFolder
            // 
            this.CbxUseCustomOutputFolder.AutoSize = true;
            this.CbxUseCustomOutputFolder.Location = new System.Drawing.Point(145, 53);
            this.CbxUseCustomOutputFolder.Name = "CbxUseCustomOutputFolder";
            this.CbxUseCustomOutputFolder.Size = new System.Drawing.Size(128, 17);
            this.CbxUseCustomOutputFolder.TabIndex = 6;
            this.CbxUseCustomOutputFolder.TabStop = false;
            this.CbxUseCustomOutputFolder.Text = "Custom Output Folder";
            this.CbxUseCustomOutputFolder.UseVisualStyleBackColor = true;
            this.CbxUseCustomOutputFolder.CheckedChanged += new System.EventHandler(this.CbxUseCustomOutputFolder_CheckedChanged);
            // 
            // TbxCustomFolder
            // 
            this.TbxCustomFolder.Location = new System.Drawing.Point(284, 51);
            this.TbxCustomFolder.Name = "TbxCustomFolder";
            this.TbxCustomFolder.ReadOnly = true;
            this.TbxCustomFolder.Size = new System.Drawing.Size(385, 20);
            this.TbxCustomFolder.TabIndex = 7;
            this.TbxCustomFolder.TabStop = false;
            // 
            // BtnSelectCustomFolder
            // 
            this.BtnSelectCustomFolder.Location = new System.Drawing.Point(675, 49);
            this.BtnSelectCustomFolder.Name = "BtnSelectCustomFolder";
            this.BtnSelectCustomFolder.Size = new System.Drawing.Size(113, 23);
            this.BtnSelectCustomFolder.TabIndex = 8;
            this.BtnSelectCustomFolder.Text = "Select Folder";
            this.BtnSelectCustomFolder.UseVisualStyleBackColor = true;
            this.BtnSelectCustomFolder.Click += new System.EventHandler(this.BtnSelectCustomFolder_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnSelectCustomFolder);
            this.Controls.Add(this.TbxCustomFolder);
            this.Controls.Add(this.CbxUseCustomOutputFolder);
            this.Controls.Add(this.CbxGenerateThumbnails);
            this.Controls.Add(this.BtnSettingsConvert);
            this.Controls.Add(this.BtnSettingsImage);
            this.Controls.Add(this.BtnSettingsThumbnail);
            this.Controls.Add(this.LblSettings);
            this.Controls.Add(this.settingsSeperator);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label settingsSeperator;
        private System.Windows.Forms.Label LblSettings;
        private System.Windows.Forms.Button BtnSettingsThumbnail;
        private System.Windows.Forms.Button BtnSettingsImage;
        private System.Windows.Forms.Button BtnSettingsConvert;
        private System.Windows.Forms.CheckBox CbxGenerateThumbnails;
        private System.Windows.Forms.CheckBox CbxUseCustomOutputFolder;
        private System.Windows.Forms.TextBox TbxCustomFolder;
        private System.Windows.Forms.Button BtnSelectCustomFolder;
    }
}