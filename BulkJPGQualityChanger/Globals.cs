﻿using System.IO;
using System.Xml.Serialization;

/* Globals.cs, BulkJPGQualityChanger
 * Globals
 * Kai Sackl, 01.05.2020
 */

namespace BulkJPGQualityChanger
{
    internal static class Globals
    {
        internal static Settings userSettings = new Settings();
        internal readonly static string settingsFilePath = "Settings.xml";
        /// <summary>
        /// Thumbnail folderpath with prefix.
        /// Path.GetTempPath() + "bulkjpg_thumbnail_"
        /// </summary>
        internal readonly static string thumbnailFolderPathWithPrefix = Path.GetTempPath() + "bulkjpg_thumbnail_";

        /// <summary>
        /// Creates the xml settings file
        /// </summary>
        internal static void CreateSettingsFile()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            using (StreamWriter streamWriter = new StreamWriter(settingsFilePath))
                xmlSerializer.Serialize(streamWriter, userSettings);
        }

        /// <summary>
        /// Load user settings from xml file
        /// </summary>
        internal static void LoadSettingsFile()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            using (FileStream fileStream = new FileStream(settingsFilePath, FileMode.Open))
                userSettings = (Settings)xmlSerializer.Deserialize(fileStream);
        }
    }
}
