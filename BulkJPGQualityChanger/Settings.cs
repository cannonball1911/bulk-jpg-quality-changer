﻿using System;

/* Settings.cs, BulkJPGQualityChanger
 * Settings
 * Kai Sackl, 01.05.2020
 */

namespace BulkJPGQualityChanger
{
    [Serializable]
    public class Settings
    {
        public bool GenerateThumbnails { get; set; }
        public bool UseCustomOutputFolder { get; set; }
        public string CustomOutputFolderPath { get; set; }

        public Settings()
        {
            GenerateThumbnails = true;
            UseCustomOutputFolder = false;
            CustomOutputFolderPath = "";
        }
    }
}
