﻿namespace BulkJPGQualityChanger
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GbxFileSelection = new System.Windows.Forms.GroupBox();
            this.LblImagesTotalLogicalFileSize = new System.Windows.Forms.Label();
            this.LblImageCount = new System.Windows.Forms.Label();
            this.BtnFileSelection = new System.Windows.Forms.Button();
            this.LbxFileSelection = new System.Windows.Forms.ListBox();
            this.PbxPreview = new System.Windows.Forms.PictureBox();
            this.GbxChanges = new System.Windows.Forms.GroupBox();
            this.LblMaxWidthHeight = new System.Windows.Forms.Label();
            this.NumMaxWidthHeight = new System.Windows.Forms.NumericUpDown();
            this.NumQuality = new System.Windows.Forms.NumericUpDown();
            this.LblQuality = new System.Windows.Forms.Label();
            this.GbxStart = new System.Windows.Forms.GroupBox();
            this.BtnStart = new System.Windows.Forms.Button();
            this.LblPreviewBoxInfo = new System.Windows.Forms.Label();
            this.MnsMenu = new System.Windows.Forms.MenuStrip();
            this.MsiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MsiFileFileSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.MsiFileSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.LblFileProgress = new System.Windows.Forms.Label();
            this.BtnCancelGenerateThumbnails = new System.Windows.Forms.Button();
            this.PgbConvertedImageProgressBar = new KaisLib.Controls.TextProgressBar();
            this.LblEstimatedTime = new System.Windows.Forms.Label();
            this.GbxFileSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbxPreview)).BeginInit();
            this.GbxChanges.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumMaxWidthHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumQuality)).BeginInit();
            this.GbxStart.SuspendLayout();
            this.MnsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // GbxFileSelection
            // 
            this.GbxFileSelection.Controls.Add(this.LblImagesTotalLogicalFileSize);
            this.GbxFileSelection.Controls.Add(this.LblImageCount);
            this.GbxFileSelection.Controls.Add(this.BtnFileSelection);
            this.GbxFileSelection.Controls.Add(this.LbxFileSelection);
            this.GbxFileSelection.Location = new System.Drawing.Point(12, 27);
            this.GbxFileSelection.Name = "GbxFileSelection";
            this.GbxFileSelection.Size = new System.Drawing.Size(535, 176);
            this.GbxFileSelection.TabIndex = 0;
            this.GbxFileSelection.TabStop = false;
            this.GbxFileSelection.Text = "1. File Selection";
            // 
            // LblImagesTotalLogicalFileSize
            // 
            this.LblImagesTotalLogicalFileSize.Location = new System.Drawing.Point(6, 39);
            this.LblImagesTotalLogicalFileSize.Name = "LblImagesTotalLogicalFileSize";
            this.LblImagesTotalLogicalFileSize.Size = new System.Drawing.Size(143, 21);
            this.LblImagesTotalLogicalFileSize.TabIndex = 7;
            this.LblImagesTotalLogicalFileSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LblImageCount
            // 
            this.LblImageCount.Location = new System.Drawing.Point(7, 21);
            this.LblImageCount.Name = "LblImageCount";
            this.LblImageCount.Size = new System.Drawing.Size(149, 21);
            this.LblImageCount.TabIndex = 6;
            this.LblImageCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnFileSelection
            // 
            this.BtnFileSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFileSelection.Location = new System.Drawing.Point(161, 19);
            this.BtnFileSelection.Name = "BtnFileSelection";
            this.BtnFileSelection.Size = new System.Drawing.Size(205, 33);
            this.BtnFileSelection.TabIndex = 1;
            this.BtnFileSelection.Text = "File Selection";
            this.BtnFileSelection.UseVisualStyleBackColor = true;
            this.BtnFileSelection.Click += new System.EventHandler(this.BtnFileSelection_Click);
            // 
            // LbxFileSelection
            // 
            this.LbxFileSelection.AllowDrop = true;
            this.LbxFileSelection.FormattingEnabled = true;
            this.LbxFileSelection.Location = new System.Drawing.Point(6, 61);
            this.LbxFileSelection.Name = "LbxFileSelection";
            this.LbxFileSelection.Size = new System.Drawing.Size(523, 108);
            this.LbxFileSelection.TabIndex = 2;
            this.LbxFileSelection.SelectedIndexChanged += new System.EventHandler(this.LbxFileSelection_SelectedIndexChanged);
            this.LbxFileSelection.DragDrop += new System.Windows.Forms.DragEventHandler(this.LbxFileSelection_DragDrop);
            this.LbxFileSelection.DragEnter += new System.Windows.Forms.DragEventHandler(this.LbxFileSelection_DragEnter);
            this.LbxFileSelection.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LbxFileSelection_KeyDown);
            this.LbxFileSelection.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LbxFileSelection_MouseDoubleClick);
            // 
            // PbxPreview
            // 
            this.PbxPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PbxPreview.Location = new System.Drawing.Point(553, 33);
            this.PbxPreview.Name = "PbxPreview";
            this.PbxPreview.Size = new System.Drawing.Size(304, 304);
            this.PbxPreview.TabIndex = 1;
            this.PbxPreview.TabStop = false;
            this.PbxPreview.DoubleClick += new System.EventHandler(this.PbxPreview_DoubleClick);
            // 
            // GbxChanges
            // 
            this.GbxChanges.Controls.Add(this.LblMaxWidthHeight);
            this.GbxChanges.Controls.Add(this.NumMaxWidthHeight);
            this.GbxChanges.Controls.Add(this.NumQuality);
            this.GbxChanges.Controls.Add(this.LblQuality);
            this.GbxChanges.Location = new System.Drawing.Point(12, 209);
            this.GbxChanges.Name = "GbxChanges";
            this.GbxChanges.Size = new System.Drawing.Size(535, 59);
            this.GbxChanges.TabIndex = 1;
            this.GbxChanges.TabStop = false;
            this.GbxChanges.Text = "2. Changes";
            // 
            // LblMaxWidthHeight
            // 
            this.LblMaxWidthHeight.AutoSize = true;
            this.LblMaxWidthHeight.Location = new System.Drawing.Point(242, 16);
            this.LblMaxWidthHeight.Name = "LblMaxWidthHeight";
            this.LblMaxWidthHeight.Size = new System.Drawing.Size(122, 13);
            this.LblMaxWidthHeight.TabIndex = 8;
            this.LblMaxWidthHeight.Text = "Max size of width/height";
            // 
            // NumMaxWidthHeight
            // 
            this.NumMaxWidthHeight.Location = new System.Drawing.Point(240, 33);
            this.NumMaxWidthHeight.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.NumMaxWidthHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumMaxWidthHeight.Name = "NumMaxWidthHeight";
            this.NumMaxWidthHeight.Size = new System.Drawing.Size(126, 20);
            this.NumMaxWidthHeight.TabIndex = 4;
            this.NumMaxWidthHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumMaxWidthHeight.Value = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            // 
            // NumQuality
            // 
            this.NumQuality.Location = new System.Drawing.Point(161, 33);
            this.NumQuality.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumQuality.Name = "NumQuality";
            this.NumQuality.Size = new System.Drawing.Size(73, 20);
            this.NumQuality.TabIndex = 3;
            this.NumQuality.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumQuality.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // LblQuality
            // 
            this.LblQuality.AutoSize = true;
            this.LblQuality.Location = new System.Drawing.Point(174, 16);
            this.LblQuality.Name = "LblQuality";
            this.LblQuality.Size = new System.Drawing.Size(39, 13);
            this.LblQuality.TabIndex = 6;
            this.LblQuality.Text = "Quality";
            // 
            // GbxStart
            // 
            this.GbxStart.Controls.Add(this.BtnStart);
            this.GbxStart.Location = new System.Drawing.Point(12, 274);
            this.GbxStart.Name = "GbxStart";
            this.GbxStart.Size = new System.Drawing.Size(535, 63);
            this.GbxStart.TabIndex = 2;
            this.GbxStart.TabStop = false;
            this.GbxStart.Text = "3. Start";
            // 
            // BtnStart
            // 
            this.BtnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStart.Location = new System.Drawing.Point(161, 19);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(205, 33);
            this.BtnStart.TabIndex = 5;
            this.BtnStart.Text = "Just Do It!";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // LblPreviewBoxInfo
            // 
            this.LblPreviewBoxInfo.Location = new System.Drawing.Point(553, 340);
            this.LblPreviewBoxInfo.Name = "LblPreviewBoxInfo";
            this.LblPreviewBoxInfo.Size = new System.Drawing.Size(304, 49);
            this.LblPreviewBoxInfo.TabIndex = 3;
            this.LblPreviewBoxInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MnsMenu
            // 
            this.MnsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MsiFile});
            this.MnsMenu.Location = new System.Drawing.Point(0, 0);
            this.MnsMenu.Name = "MnsMenu";
            this.MnsMenu.Size = new System.Drawing.Size(868, 24);
            this.MnsMenu.TabIndex = 4;
            // 
            // MsiFile
            // 
            this.MsiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MsiFileFileSelection,
            this.settingsToolStripMenuItem,
            this.MsiFileSettings});
            this.MsiFile.Name = "MsiFile";
            this.MsiFile.Size = new System.Drawing.Size(37, 20);
            this.MsiFile.Text = "&File";
            // 
            // MsiFileFileSelection
            // 
            this.MsiFileFileSelection.Name = "MsiFileFileSelection";
            this.MsiFileFileSelection.Size = new System.Drawing.Size(143, 22);
            this.MsiFileFileSelection.Text = "&File Selection";
            this.MsiFileFileSelection.Click += new System.EventHandler(this.MsiFileFileSelection_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(140, 6);
            // 
            // MsiFileSettings
            // 
            this.MsiFileSettings.Name = "MsiFileSettings";
            this.MsiFileSettings.Size = new System.Drawing.Size(143, 22);
            this.MsiFileSettings.Text = "&Settings";
            this.MsiFileSettings.Click += new System.EventHandler(this.MsiFileSettings_Click);
            // 
            // LblFileProgress
            // 
            this.LblFileProgress.Location = new System.Drawing.Point(12, 341);
            this.LblFileProgress.Name = "LblFileProgress";
            this.LblFileProgress.Size = new System.Drawing.Size(446, 21);
            this.LblFileProgress.TabIndex = 5;
            this.LblFileProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnCancelGenerateThumbnails
            // 
            this.BtnCancelGenerateThumbnails.Image = global::BulkJPGQualityChanger.Properties.Resources.button_cancel;
            this.BtnCancelGenerateThumbnails.Location = new System.Drawing.Point(832, 339);
            this.BtnCancelGenerateThumbnails.Name = "BtnCancelGenerateThumbnails";
            this.BtnCancelGenerateThumbnails.Size = new System.Drawing.Size(25, 25);
            this.BtnCancelGenerateThumbnails.TabIndex = 6;
            this.BtnCancelGenerateThumbnails.UseVisualStyleBackColor = true;
            this.BtnCancelGenerateThumbnails.Visible = false;
            this.BtnCancelGenerateThumbnails.Click += new System.EventHandler(this.BtnCancelGenerateThumbnails_Click);
            // 
            // PgbConvertedImageProgressBar
            // 
            this.PgbConvertedImageProgressBar.CustomText = "";
            this.PgbConvertedImageProgressBar.Location = new System.Drawing.Point(12, 366);
            this.PgbConvertedImageProgressBar.Name = "PgbConvertedImageProgressBar";
            this.PgbConvertedImageProgressBar.ProgressColor = System.Drawing.Color.LimeGreen;
            this.PgbConvertedImageProgressBar.Size = new System.Drawing.Size(535, 23);
            this.PgbConvertedImageProgressBar.TabIndex = 8;
            this.PgbConvertedImageProgressBar.TextColor = System.Drawing.Color.Black;
            this.PgbConvertedImageProgressBar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PgbConvertedImageProgressBar.Visible = false;
            this.PgbConvertedImageProgressBar.VisualMode = KaisLib.Controls.ProgressBarDisplayMode.Percentage;
            // 
            // LblEstimatedTime
            // 
            this.LblEstimatedTime.Location = new System.Drawing.Point(464, 341);
            this.LblEstimatedTime.Name = "LblEstimatedTime";
            this.LblEstimatedTime.Size = new System.Drawing.Size(83, 21);
            this.LblEstimatedTime.TabIndex = 9;
            this.LblEstimatedTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 393);
            this.Controls.Add(this.LblEstimatedTime);
            this.Controls.Add(this.PgbConvertedImageProgressBar);
            this.Controls.Add(this.BtnCancelGenerateThumbnails);
            this.Controls.Add(this.LblFileProgress);
            this.Controls.Add(this.LblPreviewBoxInfo);
            this.Controls.Add(this.GbxStart);
            this.Controls.Add(this.PbxPreview);
            this.Controls.Add(this.GbxChanges);
            this.Controls.Add(this.GbxFileSelection);
            this.Controls.Add(this.MnsMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.MnsMenu;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "BulkJPGQualityChanger";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.GbxFileSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PbxPreview)).EndInit();
            this.GbxChanges.ResumeLayout(false);
            this.GbxChanges.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumMaxWidthHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumQuality)).EndInit();
            this.GbxStart.ResumeLayout(false);
            this.MnsMenu.ResumeLayout(false);
            this.MnsMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox GbxFileSelection;
        private System.Windows.Forms.Button BtnFileSelection;
        private System.Windows.Forms.PictureBox PbxPreview;
        private System.Windows.Forms.ListBox LbxFileSelection;
        private System.Windows.Forms.GroupBox GbxChanges;
        private System.Windows.Forms.Label LblQuality;
        private System.Windows.Forms.GroupBox GbxStart;
        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.NumericUpDown NumQuality;
        private System.Windows.Forms.Label LblPreviewBoxInfo;
        private System.Windows.Forms.Label LblMaxWidthHeight;
        private System.Windows.Forms.NumericUpDown NumMaxWidthHeight;
        private System.Windows.Forms.MenuStrip MnsMenu;
        private System.Windows.Forms.ToolStripMenuItem MsiFile;
        private System.Windows.Forms.ToolStripMenuItem MsiFileFileSelection;
        private System.Windows.Forms.ToolStripSeparator settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MsiFileSettings;
        private System.Windows.Forms.Label LblFileProgress;
        private System.Windows.Forms.Label LblImageCount;
        private System.Windows.Forms.Label LblImagesTotalLogicalFileSize;
        private System.Windows.Forms.Button BtnCancelGenerateThumbnails;
        private KaisLib.Controls.TextProgressBar PgbConvertedImageProgressBar;
        private System.Windows.Forms.Label LblEstimatedTime;
    }
}

