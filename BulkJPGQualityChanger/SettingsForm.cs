﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

/* SettingsForm.cs, BulkJPGQualityChanger
 * The settings form
 * Kai Sackl, 08.04.2020
 */

namespace BulkJPGQualityChanger
{
    public partial class SettingsForm : Form
    {
        List<Control> controlsConvertSettings = new List<Control>();
        List<Control> controlsImageSettings = new List<Control>();
        List<Control> controlsThumbnailSettings = new List<Control>();

        public SettingsForm()
        {
            InitializeComponent();
            Init();
        }

        #region List Functions
        /// <summary>
        /// Add convert setting controlls to convert setting controlls list
        /// </summary>
        private void AddConvertSettingControlsToList()
        {
            // Use Custom Folder
            controlsConvertSettings.Add(CbxUseCustomOutputFolder);
            controlsConvertSettings.Add(TbxCustomFolder);
            controlsConvertSettings.Add(BtnSelectCustomFolder);
        }

        /// <summary>
        /// Add image setting controlls to image setting controlls list
        /// </summary>
        private void AddImageSettingControlsToList()
        {

        }

        /// <summary>
        /// Add thumbnail setting controlls to thumbnail setting controlls list
        /// </summary>
        private void AddThumbnailSettingControlsToList()
        {
            // Generate Thumbnail
            controlsThumbnailSettings.Add(CbxGenerateThumbnails);
        }
        #endregion

        #region Settings Functions
        /// <summary>
        /// Change visibility state of convert setting controls
        /// </summary>
        /// <param name="visible">true: controls visible</param>
        private void ConvertSettingsControlsVisible(bool visible)
        {
            // Use Custom Folder
            CbxUseCustomOutputFolder.Visible = visible;
            TbxCustomFolder.Visible = visible;
            BtnSelectCustomFolder.Visible = visible;
        }

        /// <summary>
        /// Change visibility state of convert setting controls
        /// </summary>
        /// <param name="visible">true: controls visible</param>
        private void ImageSettingsControlsVisible(bool visible)
        {

        }

        /// <summary>
        /// Change visibility state of convert setting controls
        /// </summary>
        /// <param name="visible">true: controls visible</param>
        private void ThumbnailSettingsControlsVisible(bool visible)
        {
            // Generate Thumbnail
            CbxGenerateThumbnails.Visible = visible;
        }
        #endregion

        #region Button Events
        private void BtnSettingsConvert_Click(object sender, EventArgs e)
        {
            HideAllSettingControls();
            ConvertSettingsControlsVisible(true);
        }

        private void BtnSettingsImage_Click(object sender, EventArgs e)
        {
            HideAllSettingControls();
            ImageSettingsControlsVisible(true);
        }

        private void BtnSettingsThumbnail_Click(object sender, EventArgs e)
        {
            HideAllSettingControls();
            ThumbnailSettingsControlsVisible(true);
        }

        private void BtnSelectCustomFolder_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    Globals.userSettings.CustomOutputFolderPath = fbd.SelectedPath;
                    TbxCustomFolder.Text = fbd.SelectedPath;
                } 
            }
        }
        #endregion

        #region Checkbox Events
        private void CbxGenerateThumbnails_CheckedChanged(object sender, EventArgs e)
        {
            Globals.userSettings.GenerateThumbnails = CbxGenerateThumbnails.Checked;
        }

        private void CbxUseCustomOutputFolder_CheckedChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TbxCustomFolder.Text) && CbxUseCustomOutputFolder.Checked)
                BtnSelectCustomFolder.PerformClick();

            Globals.userSettings.UseCustomOutputFolder = CbxUseCustomOutputFolder.Checked;
        }
        #endregion

        #region Helper Functions
        private void Init()
        {
            LoadSettings();

            // Add all setting controls to the correct list
            AddConvertSettingControlsToList();
            AddImageSettingControlsToList();
            AddThumbnailSettingControlsToList();

            // Disable all controls bevor user selects a setting topic
            HideAllSettingControls();
        }

        /// <summary>
        /// Loads the actual settings
        /// </summary>
        private void LoadSettings()
        {
            // Generate Thumbnail
            CbxGenerateThumbnails.Checked = Globals.userSettings.GenerateThumbnails;

            // Use Custom Folder
            CbxUseCustomOutputFolder.Checked = Globals.userSettings.UseCustomOutputFolder;
            TbxCustomFolder.Text = Globals.userSettings.CustomOutputFolderPath;
        }

        /// <summary>
        /// Hides all setting controls
        /// </summary>
        private void HideAllSettingControls()
        {
            ConvertSettingsControlsVisible(false);
            ImageSettingsControlsVisible(false);
            ThumbnailSettingsControlsVisible(false);
        }
        #endregion

        #region Form Closing Event
        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Globals.CreateSettingsFile();
        }
        #endregion
    }
}
