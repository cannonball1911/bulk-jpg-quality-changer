﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics; // Stopwatch
using System.Threading.Tasks;
using System.Collections.Generic;
using PhotoSauce.MagicScaler;
using System.Threading;

/* MainForm.cs, Solution: BulkJPGQualityChanger
 * Just a little tool to change the quality of jpg's in bulk
 * Kai Sackl, 24.10.2019
 */

namespace BulkJPGQualityChanger
{
    public partial class MainForm : Form
    {
        private string[] imageFilePaths;
        private DialogResult ofdResult;
        private bool thumbnailsReady = false;
        private List<string> thumbnailFilePathList;
        private CancellationTokenSource cancellationTokenSource;

        public MainForm()
        {
            InitializeComponent();
            Init();
        }

        #region Button Events
        private void BtnFileSelection_Click(object sender, EventArgs e)
        {
            FileSelectionHelperAsync();
        }

        private async void BtnStart_Click(object sender, EventArgs e)
        {
            Stopwatch sw = new Stopwatch(); // Stopwatch to count convert time
            if (LbxFileSelection.Items.Count > 0)
            {
                LockAllControls(true);
                PgbConvertedImageProgressBar.Visible = true;
                try
                {
                    Progress<double> progress = new Progress<double>(percent => { PgbConvertedImageProgressBar.Value = (int)percent; });
                    sw.Start();
                    await Task.Run(() => ConvertPictures(progress));
                    sw.Stop();
                    TimeSpan timeSpan = TimeSpan.FromMilliseconds(sw.ElapsedMilliseconds);
                    MessageBox.Show($"Elapsed Convert Time: \nHours: {timeSpan.Hours}\nMinutes: {timeSpan.Minutes}\nSeconds: {timeSpan.Seconds}", "And... its done!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "ERROR!");
                }
                PgbConvertedImageProgressBar.Visible = false;
                Clear();
                LockAllControls(false);
            }
            else
            {
                MessageBox.Show("Please choose files!", "ERROR!");
            }
        }

        private void BtnCancelGenerateThumbnails_Click(object sender, EventArgs e)
        {
            cancellationTokenSource.Cancel();
        }
        #endregion

        #region ListBox Events
        private void LbxFileSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            RenderImagePreview();
        }

        private void LbxFileSelection_DragEnter(object sender, DragEventArgs e)
        {
            string[] filepaths = (string[])e.Data.GetData(DataFormats.FileDrop);

            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) && !HasFolder(filepaths) && IsPicture(filepaths)
                ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private async void LbxFileSelection_DragDrop(object sender, DragEventArgs e)
        {
            Clear();
            string[] filepaths = (string[])e.Data.GetData(DataFormats.FileDrop);
            GetFilePathsAndAddToList(filepaths);
            foreach (string filepath in imageFilePaths)
                LbxFileSelection.Items.Add(filepath);
            
            ofdResult = DialogResult.OK;
            LbxFileSelection.SetSelected(0, true);
            BtnFileSelection.BackColor = Color.LightGreen;
            LblImageCount.Text = $"Image Count: {imageFilePaths.Length}";

            await DisplayTotalLogicalFileSizeAsync(imageFilePaths);

            if (Globals.userSettings.GenerateThumbnails)
                await ImagePreviewAsync();
        }

        private void LbxFileSelection_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int indexOfSelectedItem = LbxFileSelection.IndexFromPoint(e.Location);
            if (imageFilePaths != null && indexOfSelectedItem != ListBox.NoMatches)
                Process.Start(imageFilePaths[indexOfSelectedItem]);
        }

        private void LbxFileSelection_KeyDown(object sender, KeyEventArgs e)
        {
            int indexOfSelectedItem = LbxFileSelection.SelectedIndex;
            if (indexOfSelectedItem != ListBox.NoMatches && e.KeyCode == Keys.Enter)
                Process.Start(imageFilePaths[indexOfSelectedItem]);
        }
        #endregion

        #region PictureBox Events
        private void PbxPreview_DoubleClick(object sender, EventArgs e)
        {
            if (LbxFileSelection.SelectedIndex >= 0 && File.Exists(imageFilePaths[LbxFileSelection.SelectedIndex]))
                Process.Start(imageFilePaths[LbxFileSelection.SelectedIndex]);
        }
        #endregion

        #region MainForm Events
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (PbxPreview.Image != null)
                PbxPreview.Image.Dispose();
            if (thumbnailFilePathList != null && thumbnailFilePathList.Count > 0)
                foreach (string thumbnailPath in thumbnailFilePathList)
                {
                    if (File.Exists(thumbnailPath))
                        File.Delete(thumbnailPath);
                }
        }
        #endregion

        #region MenuStrip Events
        private void MsiFileFileSelection_Click(object sender, EventArgs e)
        {
            FileSelectionHelperAsync();
        }

        private void MsiFileSettings_Click(object sender, EventArgs e)
        {
            Form settings = new SettingsForm();
            settings.ShowDialog();
        }
        #endregion

        #region Helper Functions
        private void Init()
        {
            if (Environment.Is64BitProcess)
                Text += " x64";
            else
                Text += " x86";

            ofdResult = DialogResult.Cancel;
            thumbnailFilePathList = new List<string>();

            // User settings
            if (File.Exists(Globals.settingsFilePath))
                Globals.LoadSettingsFile();
            else
                Globals.CreateSettingsFile();
        }

        /// <summary>
        /// Gets the filepaths of the images to convert and writes the paths into an array
        /// </summary>
        private void GetImageFilePaths()
        {
            using (OpenFileDialog ofd = new OpenFileDialog {
                Multiselect = true,
                Filter = "Images|*.jpg;*.jpeg;*.png;*.png8;*.gif;*.bmp;*.tif;*.tiff|" +
                "JPG|*.jpg;*.jpeg|PNG|*.png;*.png8|GIF|*.gif|BMP|*.bmp|TIFF|*.tif;*.tiff"
            })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    ofdResult = DialogResult.OK;
                    BtnFileSelection.BackColor = Color.LightGreen;
                    GetFilePathsAndAddToList(ofd.FileNames);
                    LbxFileSelection.DataSource = imageFilePaths;
                    return;
                }
                ofdResult = DialogResult.Cancel;
                Clear();
            }
        }

        /// <summary>
        /// Gets the filepaths of the images and adds it to an array
        /// </summary>
        /// <param name="stringArray"></param>
        private void GetFilePathsAndAddToList(string[] stringArray)
        {
            imageFilePaths = new string[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++)
                imageFilePaths[i] = stringArray[i];
        }

        /// <summary>
        /// Renders the preview image which is selected in the listbox to the picturebox
        /// </summary>
        private void RenderImagePreview() 
        {
            if (ofdResult == DialogResult.OK && thumbnailsReady)
            {
                if (PbxPreview.Image != null)
                    PbxPreview.Image.Dispose();
                if (LbxFileSelection.SelectedIndex < 0)
                    return;
                try
                {                    
                    PbxPreview.Image = Image.FromFile(Globals.thumbnailFolderPathWithPrefix + Path.GetFileName(imageFilePaths[LbxFileSelection.SelectedIndex]));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "ERROR!");
                }
            }
        }

        /// <summary>
        /// Generates the thumbnails for all images in imageFilePaths array
        /// </summary>
        /// <returns></returns>
        private Task<bool> GenerateThumbnails()
        {
            LockControlsOnGeneratingThumbnails(true);

            cancellationTokenSource = new CancellationTokenSource();
            Task<bool> task = Task.Run(() =>
            {
                foreach (string img in imageFilePaths)
                {
                    if (cancellationTokenSource.Token.IsCancellationRequested)
                    {
                        cancellationTokenSource.Dispose();
                        LockControlsOnGeneratingThumbnails(false);
                        return false;
                    }

                    string fullThumbnailFilePath = Globals.thumbnailFolderPathWithPrefix + Path.GetFileName(img);

                    if (!File.Exists(fullThumbnailFilePath))
                    {
                        ProcessImageSettings imageSettings = new ProcessImageSettings { SaveFormat = FileFormat.Jpeg };
                        ImageFileInfo imageInfo = ImageFileInfo.Load(img);

                        // Resize to PbxPreview size
                        if (imageInfo.Frames[0].Width > imageInfo.Frames[0].Height)
                            imageSettings.Width = PbxPreview.Width;
                        else
                            imageSettings.Height = PbxPreview.Height;

                        using (FileStream outStream = new FileStream(fullThumbnailFilePath, FileMode.Create))
                        {
                            MagicImageProcessor.ProcessImage(img, outStream, imageSettings);
                            thumbnailFilePathList.Add(fullThumbnailFilePath);
                        }
                    }
                }
                LockControlsOnGeneratingThumbnails(false);
                return true;
            }, cancellationTokenSource.Token);

            return task;
        }

        /// <summary>
        /// Handels the image preview
        /// </summary>
        /// <returns></returns>
        private async Task ImagePreviewAsync()
        {
            if (imageFilePaths != null && imageFilePaths.Length > 0)
            {
                BtnCancelGenerateThumbnails.Visible = true;
                PbxPreview.SizeMode = PictureBoxSizeMode.CenterImage;
                LblPreviewBoxInfo.Text = "Please wait...\n Thumbnails are generated :)";
                thumbnailsReady = await GenerateThumbnails();
                LblPreviewBoxInfo.Text = "";
                RenderImagePreview();
                BtnCancelGenerateThumbnails.Visible = false;
            }
        }

        /// <summary>
        /// Converts the images in imageFilePaths array
        /// </summary>
        /// <param name="progress">Reports converted images progress in percent</param>
        private void ConvertPictures(IProgress<double> progress)
        {
            int currentlyConvertedImage = 0;
            ETACalculator estimatedTimeCalculator = new ETACalculator(imageFilePaths);

            foreach (string img in imageFilePaths)
            {
                currentlyConvertedImage++;
                estimatedTimeCalculator.Begin();

                ImageFileInfo imageInfo = ImageFileInfo.Load(img);
                ProcessImageSettings newImageSettings = new ProcessImageSettings
                {
                    SaveFormat = FileFormat.Jpeg,
                    JpegQuality = (int)NumQuality.Value
                };

                // Resize image if image width/height is bigger NumMaxWidthHeight.Value
                if ((imageInfo.Frames[0].Width > (int)NumMaxWidthHeight.Value
                    || imageInfo.Frames[0].Height > (int)NumMaxWidthHeight.Value)
                    && (imageInfo.Frames[0].Width > imageInfo.Frames[0].Height))
                {
                    newImageSettings.Width = (int)NumMaxWidthHeight.Value;
                }
                else if ((imageInfo.Frames[0].Width > (int)NumMaxWidthHeight.Value
                    || imageInfo.Frames[0].Height > (int)NumMaxWidthHeight.Value)
                    && (imageInfo.Frames[0].Width < imageInfo.Frames[0].Height))
                {
                    newImageSettings.Height = (int)NumMaxWidthHeight.Value;
                }

                string rootImgDirectory = Path.GetDirectoryName(img);
                string imgFileName = Path.GetFileNameWithoutExtension(img);
                string outputFolder = $@"{rootImgDirectory}\BulkJPG_Converted";

                if (Globals.userSettings.UseCustomOutputFolder)
                    outputFolder = Globals.userSettings.CustomOutputFolderPath;

                if (!Directory.Exists(outputFolder))
                    Directory.CreateDirectory(outputFolder);

                LblFileProgress.Invoke((MethodInvoker)delegate { LblFileProgress.Text = $"{currentlyConvertedImage} ---> {img}"; });

                using (FileStream outStream = new FileStream($@"{outputFolder}\{imgFileName}.jpg", FileMode.Create))
                {
                    MagicImageProcessor.ProcessImage(img, outStream, newImageSettings);
                }

                // Calculate & report progress in percent
                double progressInPercent = Math.Round((double)(currentlyConvertedImage * 100) / imageFilePaths.Length, 0);
                if (progress != null)
                    progress.Report(progressInPercent);

                // Get estimated process time
                TimeSpan timeSpan = TimeSpan.FromSeconds(estimatedTimeCalculator.EstimatedProcessTimeInSeconds);
                LblFileProgress.Invoke((MethodInvoker)delegate { LblEstimatedTime.Text = $@"ETA: {timeSpan:hh\:mm\:ss}"; });
            }
        }

        /// <summary>
        /// Clears the listbox, preview, labels for file progress & image count
        /// and resets the fileselection button
        /// </summary>
        private void Clear()
        {
            LbxFileSelection.DataSource = null;
            LbxFileSelection.Items.Clear();
            if (PbxPreview.Image != null)
            {
                PbxPreview.Image.Dispose();
                PbxPreview.Image = null;
            }
            SetButtonDefaultColor(BtnFileSelection);
            thumbnailsReady = false;
            LblFileProgress.Text = "";
            LblImageCount.Text = "";
            LblImagesTotalLogicalFileSize.Text = "";
            PgbConvertedImageProgressBar.Value = 0;
            LblEstimatedTime.Text = "";
        }

        /// <summary>
        /// Enables or disables all controls for user interaction
        /// </summary>
        /// <param name="locked">true: Controlls locked</param>
        private void LockAllControls(bool locked)
        {
            BtnFileSelection.Enabled = !locked;
            BtnStart.Enabled = !locked;
            NumQuality.Enabled = !locked;
            NumMaxWidthHeight.Enabled = !locked;
        }

        /// <summary>
        /// Sets a button to default color
        /// </summary>
        /// <param name="button"></param>
        private void SetButtonDefaultColor(Button button)
        {
            button.BackColor = SystemColors.ButtonFace;
            button.ForeColor = default(Color);
            button.UseVisualStyleBackColor = true;
        }

        /// <summary>
        /// Checks if array of filepaths contains a folder
        /// </summary>
        /// <param name="stringArray"></param>
        /// <returns>true: Array contains folder</returns>
        private bool HasFolder(string[] stringArray)
        {
            foreach (string fullFileName in stringArray)
                if (File.GetAttributes(fullFileName).HasFlag(FileAttributes.Directory))
                    return true;
            return false;
        }

        /// <summary>
        /// Checks if array of filepaths are all pictures
        /// </summary>
        /// <param name="stringArray"></param>
        /// <returns>true: Array contains only pictures</returns>
        private bool IsPicture(string[] stringArray)
        {
            foreach (string fullFileName in stringArray)
            {
                string ext = Path.GetExtension(fullFileName).ToLower();
                if (ext != ".jpg" && ext != ".jpeg" && ext != ".png" && ext != ".png8" &&
                    ext != ".gif" && ext != ".bmp" && ext != ".tif" && ext != ".tiff")
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Helper for when file selection happens
        /// </summary>
        private async void FileSelectionHelperAsync()
        {
            Clear();
            GetImageFilePaths();
            if (imageFilePaths != null && imageFilePaths.Length > 0 && LbxFileSelection.Items.Count > 0)
            {
                LblImageCount.Text = $"Image Count: {imageFilePaths.Length}";
                await DisplayTotalLogicalFileSizeAsync(imageFilePaths);
            }
            if (Globals.userSettings.GenerateThumbnails)
                await ImagePreviewAsync();
        }

        /// <summary>
        /// Gets the total logical file size of all provided images
        /// </summary>
        /// <param name="images">array of filepaths to images</param>
        /// <returns></returns>
        private long GetTotalLogicalFileSizeInBytes(string[] images)
        {
            long totalLogicalFileSize = 0;
            foreach (string img in images)
                totalLogicalFileSize += new FileInfo(img).Length;
            return totalLogicalFileSize;
        }

        /// <summary>
        /// Displays the total logical size of all provided images
        /// </summary>
        /// <param name="images">array of filepaths to images</param>
        private async Task DisplayTotalLogicalFileSizeAsync(string[] images)
        {
            long totalLogicalFileSize = await Task.Run(() => GetTotalLogicalFileSizeInBytes(images));
            double totalMegabyte = (double)totalLogicalFileSize / 1024 / 1024;
            double totalGigabyte = (double)totalLogicalFileSize / 1024 / 1024 / 1024;
            if (totalMegabyte < 1000)
                LblImagesTotalLogicalFileSize.Text = $"Total FileSize: {Math.Round(totalMegabyte, 1)} MB";
            else
                LblImagesTotalLogicalFileSize.Text = $"Total FileSize: {Math.Round(totalGigabyte, 2)} GB";
        }

        /// <summary>
        /// Lock file selection controls on generating thumbnails
        /// </summary>
        /// <param name="locked">true: controls locked</param>
        private void LockControlsOnGeneratingThumbnails(bool locked)
        {
            if (BtnFileSelection.InvokeRequired)
                BtnFileSelection.Invoke((MethodInvoker)delegate { BtnFileSelection.Enabled = !locked; });
            else
                BtnFileSelection.Enabled = !locked;

            if (LbxFileSelection.InvokeRequired)
                LbxFileSelection.Invoke((MethodInvoker)delegate { LbxFileSelection.Enabled = !locked; });
            else
                LbxFileSelection.Enabled = !locked;
        }
        #endregion
    }
}
